package com.bilegole.extraone.sso_oauth2_client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author yuang
 */
@SpringBootApplication
public class SsoOauth2Client {

    public static void main(String[] args) {
        SpringApplication.run(SsoOauth2Client.class, args);
    }

}
